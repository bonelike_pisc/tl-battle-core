use std::rc::Rc;
use tl_battle_core::*;
use tl_battle_core::cmd_skill::*;
use tl_battle_core::ai::*;
use tl_battle_core::event::EventQueue;

#[test]
fn test_even_ai() {
    let context = BattleEnv::new(make_context());
    let executor = context.borrow().participants.get(0).unwrap().clone();
    use tl_battle_core::ai::AIPattern;
    let table = EvenAI.generate_probabilities(executor, context);
    for i in -1..35 {
        let rv = i as f32 * 0.03;
        let action = table.get(rv);
        match action {
            Ok(a) => println!("{}: {}", rv, action_to_string(&a)),
            Err(e) => println!("{}: {}", rv, e.to_string())
        };
    }

    println!("### random test ###");
    let mut random = RandomPicker::default();
    for i in 0..100 {
        let action = random.pick(&table).unwrap();
        println!("r{}: {}", i + 1, action_to_string(&action));
    }
}

fn make_context() -> BattleEntity {
    let p1 = ParticipantStaticEnt {
		name: "executor".to_string(),
        group_id: 0,
        is_playable: false,
        ai_pattern: Some(Box::new(EvenAI)),
        level: 5,
        power: 5,
        defence: 5,
        endurance: 5,
        speed: 5,
        skill_level: 5,
		command_skills: vec![
			Rc::new(AttackConstantDamage::new(
        		String::from("attack1"), 4, 200
    		)),
            Rc::new(AttackConstantDamage::enemy_all(
        		String::from("attack2"), 6, 320
            )),
            Rc::new(RecoverConstant::myself(
                String::from("recover1"), 7, 100
            )),
            Rc::new(RecoverConstant::friend1(
                String::from("recover2"), 7, 100
            )),
            Rc::new(RecoverConstant::friend_all(
                String::from("recover3"), 7, 100
            )),
		],
		passive_skills: Vec::new()
	};
	let p2 = ParticipantStaticEnt {
		name: "friendA".to_string(),
        group_id: 0,
        is_playable: false,
        ai_pattern: None,
        level: 5,
        power: 5,
        defence: 5,
        endurance: 5,
        speed: 5,
        skill_level: 5,
		command_skills: Vec::new(),
		passive_skills: Vec::new()
    };
    let p3 = ParticipantStaticEnt {
		name: "friendB".to_string(),
        group_id: 0,
        is_playable: false,
        ai_pattern: None,
        level: 5,
        power: 5,
        defence: 5,
        endurance: 5,
        speed: 5,
        skill_level: 5,
		command_skills: Vec::new(),
		passive_skills: Vec::new()
    };
    let p4 = ParticipantStaticEnt {
		name: "enemyA".to_string(),
        group_id: 1,
        is_playable: false,
        ai_pattern: None,
        level: 5,
        power: 5,
        defence: 5,
        endurance: 5,
        speed: 5,
        skill_level: 5,
		command_skills: Vec::new(),
		passive_skills: Vec::new()
    };
    let p5 = ParticipantStaticEnt {
		name: "enemyB".to_string(),
        group_id: 1,
        is_playable: false,
        ai_pattern: None,
        level: 5,
        power: 5,
        defence: 5,
        endurance: 5,
        speed: 5,
        skill_level: 5,
		command_skills: Vec::new(),
		passive_skills: Vec::new()
    };
    
    let participants: Vec<Participant> = vec![p1, p2, p3, p4, p5]
        .drain(..).map(|p| {
            Participant {
                static_ent: Rc::new(p),
                dynamic_ent: Rc::new(ParticipantDynamicEnt::new())
            }
        }).collect();

    // 選択されないパターン用
    //participants[2].dynamic_ent.hp.set(0);
    //participants[4].dynamic_ent.hp.set(0);

    BattleEntity {
        participants: participants,
        event: EventQueue::new()
    }
}

fn action_to_string(action: &Action) -> String {
    let mut target_str = action.targets.get(0).unwrap().static_ent.name.to_string();
    for i in 1..action.targets.len() {
        target_str = format!("{},{}", target_str, action.targets.get(i).unwrap().static_ent.name.to_string());
    }
    format!("{{ command:{}, target:{} }}", action.command.get_name(), target_str)
}