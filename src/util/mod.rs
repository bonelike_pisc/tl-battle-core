pub mod numeric;
pub mod random;

use std::rc::Rc;
use std::cell::RefCell;

/* ユーティリティ関数群 */

/*
 * 整数をindex値として使うため、i32 -> usizeに変換。
 * 変換できない場合はusize型の最大値。
 * （そんなindex値はまずありえないので範囲チェックで引っかかるでしょw）
 */
pub fn get_index(i: i32) -> usize {
    use std::convert::TryFrom;
    usize::try_from(i).unwrap_or(std::usize::MAX)
}

pub struct OptionRc<T: ?Sized> {
    o: RefCell<Option<Rc<T>>>
}

impl<T: ?Sized> OptionRc<T> {
    pub fn none() -> Self { 
        Self { o: RefCell::new(Option::None) }
    }
    pub fn new(rc: Rc<T>) -> Self {
        Self { o: RefCell::new(Option::Some(rc)) }
    }
    pub fn set(&self, rc: Rc<T>) {
        *self.o.borrow_mut() = Some(rc);
    }
    pub fn clear(&self) {
        *self.o.borrow_mut() = None;
    }
    pub fn get(&self) -> Option<Rc<T>> {
        self.o.borrow().as_ref().map(|rc| rc.clone())
    }
}
