use rand::{Rng, SeedableRng};
use rand::rngs::SmallRng;

pub fn get_random_tf(true_prob: f32) -> bool {
    let value = get_value_between_0_1();
    value <= true_prob
}

pub fn get_random_value_between_range(min: f32, max: f32) -> f32 {
    let value = get_value_between_0_1();
    min + (max - min) * value
}

fn get_value_between_0_1() -> f32 {
    let mut thread_rng = rand::thread_rng();
    let mut rng = SmallRng::from_rng(&mut thread_rng).unwrap();
    rng.gen()
}