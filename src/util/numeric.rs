

pub fn generate_sigmoid(left_anchor_x: f32, left_anchor_y: f32, right_anchor_x: f32, right_anchor_y: f32)
        -> Result<Box<dyn Fn(f32) -> f32>, String> {
    if left_anchor_x == right_anchor_x {
        return Err(String::from("invalid parameter"));
    }

    let func = move |x| {
        // sigmoid likeな関数
        let x1 = &left_anchor_x;
        let y1 = &left_anchor_y;
        let x2 = &right_anchor_x;
        let y2 = &right_anchor_y;
        if &x <= x1 { return *y1; }
        if &x >= x2 { return *y2; }
        let mut ret = 0.0;
        let x_shifted = x - (x1 + x2) / 2.0;
        ret += -2.0 * (y2 - y1) / ((x2 - x1) * (x2 - x1) * (x2 - x1)) * x_shifted * x_shifted * x_shifted;
        ret += 1.5 * (y2 - y1) / (x2 - x1) * x_shifted;
        ret += (y1 + y2) / 2.0;
        ret
    };
    Ok(Box::new(func))
}
