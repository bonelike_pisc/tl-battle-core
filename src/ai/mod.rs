mod prob_table;
mod random;
mod even_ai;
pub use prob_table::*;
pub use random::*;
pub use even_ai::*;

use std::rc::Rc;
use crate::{Participant, BattleEnv};
use crate::cmd_skill::CommandSkill;
use self::ProbabilityTable;

pub trait AIPattern {
    fn generate_probabilities(&self, executor: Participant, context: BattleEnv) -> ProbabilityTable<Action>;
}

#[derive(Clone)]
pub struct Action {
    pub command: Rc<dyn CommandSkill>,
    pub targets: Vec<Participant>,
}