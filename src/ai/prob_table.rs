
pub struct ProbabilityTable<T> where T: Clone {
    choice: Vec<T>, //選択肢
    probabilities: Vec<f32> //確率指数（正規化するので合計は1でなくてもよい）
}

impl <T> ProbabilityTable<T> where T: Clone {
    pub fn new() -> Self {
        Self {
            choice: Vec::new(),
            probabilities: Vec::new(),
        }
    }

    pub fn append(&mut self, choice: T, prob: f32) {
        self.choice.push(choice);
        self.probabilities.push(prob);
    }

    pub fn get(&self, key: f32) -> Result<T, String> {
        if key < 0.0 || 1.0 <= key {
            return Err("random key must be in [0, 1)".to_string());
        }
        if self.choice.len() == 0 {
            return Err("no choice set.".to_string());
        }

        let key_scaled = key * self.scale(); //scale倍しておく（keyを正規化）

        let mut sum = 0.0;
        for i in 0..self.probabilities.len() {
            sum += self.probabilities.get(i).unwrap();
            if key_scaled <= sum {
                return Ok(self.choice.get(i).unwrap().clone());
            }
        }

        // まだkey > sumの場合、誤差だと思うので最後の選択肢を返す
        Ok(self.choice.last().unwrap().clone())
    }

    pub fn is_empty(&self) -> bool {
        self.choice.len() == 0
    }

    // 確率指数の合計を計算
    fn scale(&self) -> f32 {
        let mut sum = 0.0;
        for p in &self.probabilities {
            sum += p;
        }
        sum
    }
}