use super::{AIPattern, Action, ProbabilityTable};
use crate::cmd_skill::TargetType;
use crate::{Participant, BattleEnv};

// スキル・対象それぞれ等確率の確率テーブルを生成
pub struct EvenAI;

impl AIPattern for EvenAI {
    fn generate_probabilities(&self, executor: Participant, context: BattleEnv) -> ProbabilityTable<Action> {
        let mut ret = ProbabilityTable::new();
        let commands = executor.get_selectable_commands();
        for c in &commands {
            let target_type = c.get_target_type();
            match target_type {
                TargetType::Friend1 | TargetType::Enemy1 => {
                    let targets = target_type.get_selectable_participants(executor.clone(), &context.borrow().participants);
                    ////let targets = c.get_target_filter().filter_for_select(&_targets);
                    //選択可能な対象がいない場合は選択肢に入れない
                    if targets.len() == 0 { continue; }

                    for t in &targets {
                        let action = Action {
                            command: c.clone(),
                            targets: vec![t.clone()],
                        };
                        let prob = 1.0 / targets.len() as f32;
                        ret.append(action, prob);
                    }
                },
                TargetType::Myself | TargetType::FriendAll | TargetType::EnemyAll => {
                    let targets = target_type.get_selectable_participants(executor.clone(), &context.borrow().participants);
                    ////let targets = c.get_target_filter().filter_for_select(&_targets);
                    //対象がいない場合は選択肢に入れない
                    if targets.len() == 0 { continue; }

                    let action = Action {
                        command: c.clone(),
                        targets: targets,
                    };
                    ret.append(action, 1.0);
                },
            }
        }

        ret
    }
}
