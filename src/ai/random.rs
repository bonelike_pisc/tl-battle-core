use rand::{Rng, SeedableRng};
use rand::rngs::SmallRng;
use super::ProbabilityTable;

pub struct RandomPicker<R> where R: Rng {
    rng: R
}

impl <R> RandomPicker<R> where R: Rng {
    pub fn from_rng(rng: R) -> Self {
        Self { rng: rng }
    }

    pub fn pick<T>(&mut self, prob_table: &ProbabilityTable<T>) -> Result<T, String> where T: Clone {
        let random_value: f32 = self.rng.gen();
        prob_table.get(random_value)
    }
}

impl RandomPicker<SmallRng> {
    pub fn default() -> Self {
        let mut thread_rng = rand::thread_rng();
        let rng = SmallRng::from_rng(&mut thread_rng).unwrap();
        Self { rng: rng }
    }
}