use crate::participant::ParticipantDynamicEnt;

pub trait PassiveSkill {
    fn apply<'a>(&self, owner: &'a mut ParticipantDynamicEnt);
}
