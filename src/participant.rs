use std::rc::Rc;
use std::cell::{Cell, RefCell};
use crate::task::TaskPool;
use crate::cmd_skill::CommandSkill;
use crate::psv_skill::PassiveSkill;
use crate::ai::AIPattern;

pub const MAX_HP: u16 = 1000;
pub const MAX_SP: u16 = 1000;

// バトル参加者エンティティ
#[derive(Clone)]
pub struct Participant {
    pub static_ent: Rc<ParticipantStaticEnt>,
    pub dynamic_ent: Rc<ParticipantDynamicEnt>,
}

// 静的データ
pub struct ParticipantStaticEnt {
    pub name: String, //ID
    pub group_id: u8, //敵味方の区別
    pub is_playable: bool, //操作可能かどうか
    pub ai_pattern: Option<Box<dyn AIPattern>>, //自動キャラの場合の行動パターン
    pub level: u8,
    pub power: u16, //魔力
    pub defence: u16, //耐久力
    pub endurance: u16, //持久力
    pub speed: u16, //詠唱速度
    pub skill_level: u16, //技量
    pub passive_skills: Vec<Rc<dyn PassiveSkill>>,
    pub command_skills: Vec<Rc<dyn CommandSkill>>
}

// 動的データ
pub struct ParticipantDynamicEnt {
    pub hp: Cell<u16>,
    pub sp: Cell<u16>,
    pub knockdown_flag: Cell<bool>, //ノックダウン済みかどうか
    pub tasks: RefCell<TaskPool>, //タスク
}

impl Participant {
    pub fn new_full(p: ParticipantStaticEnt) -> Self {
        Self {
            static_ent: Rc::new(p),
            dynamic_ent: Rc::new(ParticipantDynamicEnt::new())
        }
    }

    pub fn get_name(&self) -> &String {
        &self.static_ent.name
    }

    pub fn get_hp(&self) -> u16 {
        self.dynamic_ent.hp.get()
    }

    pub fn set_hp(&mut self, value: u16) {
        self.dynamic_ent.hp.set(value);
    }

    pub fn get_sp(&self) -> u16 {
        self.dynamic_ent.sp.get()
    }

    pub fn set_sp(&mut self, value: u16) {
        self.dynamic_ent.sp.set(value)
    }

    pub fn is_free(&self) -> bool {
        self.dynamic_ent.tasks.borrow().get_tasks().len() == 0
    }

    //行動可能かどうか
    pub fn can_act(&self) -> bool {
        self.is_alive() && self.is_free() && self.get_selectable_commands().len() > 0
    }

    // 生存しているかどうか
    pub fn is_alive(&self) -> bool {
        self.dynamic_ent.hp.get() > 0
    }

    // 同じグループかどうか判定
    pub fn is_same_group(p1: &Participant, p2: &Participant) -> bool {
        p1.static_ent.group_id == p2.static_ent.group_id
    }

    // 選択可能なスキル一覧を取得
    pub fn get_selectable_commands(&self) -> Vec<Rc<dyn CommandSkill>> {
        //今は所持スキル一覧をそのまま返す
        self.static_ent.command_skills.clone()
    }
}

impl PartialEq for Participant {
    fn eq(&self, other: &Self) -> bool {
        Rc::ptr_eq(&self.static_ent, &other.static_ent)
            && Rc::ptr_eq(&self.dynamic_ent, &other.dynamic_ent)
    }
}


impl ParticipantDynamicEnt {
    // 初期化
    pub fn new() -> Self {
        Self {
            hp: Cell::new(MAX_HP), //max value
            sp: Cell::new(MAX_SP), //max value
            knockdown_flag: Cell::new(false),
            tasks: RefCell::new(TaskPool::new()),
        }
    }
}
