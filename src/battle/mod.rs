use std::rc::Rc;
use std::cell::{RefCell, Ref, RefMut};
use crate::participant::*;
use crate::state::StateEnum;
use crate::event::EventQueue;

// データ実体
pub struct BattleEntity {
    pub participants: Vec<Participant>,
    pub event: EventQueue,
}

// BattleEntityのラッパー
#[derive(Clone)]
pub struct BattleEnv {
    ent: Rc<RefCell<BattleEntity>>
}

impl BattleEnv {
    pub fn new(ent: BattleEntity) -> Self {
        Self {
            ent: Rc::new(RefCell::new(ent))
        }
    }

    pub fn borrow(&self) -> Ref<BattleEntity> {
        self.ent.borrow()
    }

    pub fn borrow_mut(&mut self) -> RefMut<BattleEntity> {
        self.ent.borrow_mut()
    }

    pub fn get_participants(&self) -> Vec<Participant> {
        self.ent.borrow().participants.clone()
    }

    // 参加者のgroup_id一覧を重複なく取得
    pub fn get_participant_groups(&self) -> Vec<u8> {
        let mut ret = Vec::new();
        for p in &self.borrow().participants {
            if !ret.contains(&p.static_ent.group_id) {
                ret.push(p.static_ent.group_id);
            }
        }
        ret
    }
    
    // グループの1人以上生き残っているかどうか判定
    pub fn group_is_alive(&self, group_id: u8) -> bool {
        self.borrow().participants.iter()
            .filter(|&p| p.static_ent.group_id == group_id && p.is_alive())
            .next()
            .is_some()
    }
}

// フロントエンド
pub struct BattleRunner {
    env: BattleEnv,
    state: Option<StateEnum>,
}

impl BattleRunner {
    // 新規作成
    pub fn new(participants: Vec<Participant>) -> Self {
        let ent = BattleEntity {
            participants: participants,
            event: EventQueue::new(),
        };
        let env = BattleEnv::new(ent);

        Self {
            env: env.clone(),
            state: Some(StateEnum::new_post_initiate(env)),
        }
    }

    // 全員全快の状態から開始
    pub fn new_full(mut participants: Vec<ParticipantStaticEnt>) -> Self {
        let m_participants = participants.drain(..).map(|p| Participant::new_full(p)).collect();
        Self::new(m_participants)
    }

    // 別の状態からの再作成
    pub fn renew(env: BattleEnv, state: StateEnum) -> Self {
        Self {
            env: env,
            state: Some(state),
        }
    }

    // 現在のStateを取得（参照用）
    pub fn get_state(&self) -> Option<&StateEnum> {
        self.state.as_ref()
    }

    // 現在のStateを取得（更新用）
    pub fn take_state(&mut self) -> Option<StateEnum> {
        self.state.take()
    }

    pub fn get_participants(&self) -> Vec<Participant> {
        self.env.borrow().participants.clone()
    }

}
