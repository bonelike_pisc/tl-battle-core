use crate::{participant, Participant};
use crate::battle::BattleEnv;
use super::{TaskBase, TaskInfo};
use crate::event::{Event, EventInfo};
use crate::util::numeric;

// コマンド実行準備タスク
pub struct ExecutionPrepareTask {
    event: Event,
    remain_cost: u16,
    whole_cost: u16,
}

impl ExecutionPrepareTask {
    pub fn new(execution: Event) -> Self {
        let remain_cost = get_cost(&execution).unwrap();
        Self {
            event: execution,
            remain_cost: remain_cost,
            whole_cost: remain_cost,
        }
    }
}

impl TaskBase for ExecutionPrepareTask {
    fn proceed(&mut self) {
        let mut executor = get_executor(&self.event).unwrap();
        // 詠唱速度と残スタミナ割合から消費コストを算出
        let sigmoid_fn = numeric::generate_sigmoid(0.0, 0.0, 0.5, 1.0).unwrap(); //曲線関数を生成
        let consume_cost_f = executor.static_ent.speed as f32 * (0.2 + 0.8 * sigmoid_fn(executor.get_sp() as f32 / participant::MAX_SP as f32));
        let mut consume_cost = consume_cost_f as u16;
        // 残コストを消費しきれる場合は残コスト分だけ消費する
        if consume_cost > self.remain_cost {
            consume_cost = self.remain_cost;
        }
        let mut consume_sp = consume_cost * 20 / executor.static_ent.endurance;
        // スタミナが足りない場合はある分だけ消費。
        if consume_sp > executor.get_sp() {
            consume_sp = executor.get_sp();
        }
        
        self.remain_cost -= consume_cost;
        executor.set_sp(executor.get_sp() - consume_sp);
    }

    fn is_finished(&self) -> bool {
        self.remain_cost == 0
    }

    fn on_finished(&mut self, env: &mut BattleEnv) {
        env.borrow_mut().event.push(self.event.clone());
    }

    fn get_info(&self) -> TaskInfo {
        TaskInfo::ExecutionPrepare(self.event.clone(), self.remain_cost, self.whole_cost)
    }
}

fn get_executor(event: &Event) -> Result<Participant, String> {
    match event.get_info() {
        EventInfo::Execution(_cmd, executor, _targets) => Ok(executor),
        _ => Err("unexpected event type".to_string())
    }
}

fn get_cost(event: &Event) -> Result<u16, String> {
    match event.get_info() {
        EventInfo::Execution(cmd, _executor, _targets) => {
            // 実行者の魔力にも依存
            //let value = executor.static_ent.power as f32 * cmd.get_cost() as f32;
            //Ok(value as u16)

            // 魔力に依存しないバージョン
            let value = cmd.get_cost() * 10;
            Ok(value)
        },
        _ => Err("unexpected event type".to_string())
    }
}
