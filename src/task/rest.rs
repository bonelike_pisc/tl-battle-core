use crate::{participant, Participant, BattleEnv};
use super::{TaskBase, TaskInfo};

pub struct RestTask {
    executor: Participant,
    turns: u16,
    recovery_rate: u16,
}

impl RestTask {
    pub fn new(executor: Participant, turns: u16, recovery_rate: u16) -> Self {
        Self { executor, turns, recovery_rate }
    }
}

impl TaskBase for RestTask {
    fn proceed(&mut self) {
        let mut after_sp = self.executor.get_sp() + self.recovery_rate;
        // 上限に達する場合は上限まで回復し、即完了とする。
        if after_sp > participant::MAX_SP {
            after_sp = participant::MAX_SP;
        }
        self.executor.set_sp(after_sp);
        if after_sp == participant::MAX_SP {
            self.turns = 0;
        }
        else {
            self.turns -= 1;
        }
    }

    fn is_finished(&self) -> bool {
        self.turns == 0
    }

    fn on_finished(&mut self, _env: &mut BattleEnv) {
        //特に何もしない
    }

    fn get_info(&self) -> TaskInfo {
        TaskInfo::Rest(self.executor.clone(), self.turns, self.recovery_rate)
    }
}
