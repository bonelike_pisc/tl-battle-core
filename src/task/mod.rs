mod prepare;
mod rest;
pub use prepare::*;
pub use rest::*;

use std::rc::Rc;
use std::cell::RefCell;
use crate::{Participant, BattleEnv};
use crate::event::Event;

pub trait TaskBase {
    fn proceed(&mut self); //1ターン経過する際の処理
    fn is_finished(&self) -> bool; //完了したかどうか
    fn on_finished(&mut self, env: &mut BattleEnv); //完了した際の処理
    fn get_info(&self) -> TaskInfo; //情報取得
}

#[derive(Clone)]
pub struct Task {
    t: Rc<RefCell<dyn TaskBase>>
}

impl Task {
    pub fn new<T>(t: T) -> Self where T: TaskBase + 'static {
        Self {
            t: Rc::new(RefCell::new(t))
        }
    }

    pub fn proceed(&mut self) {
        self.t.borrow_mut().proceed();
    }

    pub fn is_finished(&self) -> bool {
        self.t.borrow().is_finished()
    }

    pub fn on_finished(&mut self, env: &mut BattleEnv) {
        self.t.borrow_mut().on_finished(env);
    }

    pub fn get_info(&self) -> TaskInfo {
        self.t.borrow().get_info()
    }
}

impl PartialEq for Task {
    fn eq(&self, other: &Self) -> bool {
        Rc::ptr_eq(&self.t, &other.t)
    }
}

pub enum TaskInfo {
    ExecutionPrepare(Event, u16, u16),
    Rest(Participant, u16, u16),
}

pub struct TaskPool {
    tasks: Vec<Task>
}

impl TaskPool {
    pub fn new() -> Self {
        Self {
            tasks: Vec::new()
        }
    }

    pub fn add_task(&mut self, task: Task) {
        self.tasks.push(task);
    }

    pub fn update(&mut self, env: &mut BattleEnv) {
        let mut to_delete = Vec::new();
        for task in &mut self.tasks {
            task.proceed();
            if task.is_finished() {
                task.on_finished(env);
                to_delete.push(task.clone());
            }
        }

        self.tasks.retain(|t| !to_delete.contains(t));
    }

    pub fn clear(&mut self) {
        self.tasks.clear();
    }

    pub fn get_tasks(&self) -> &Vec<Task> {
        &self.tasks
    }
}
