mod initiate;
mod action_select;
mod command_select;
mod target_select;
mod event_register;
mod rest_start;
mod event_execute;
mod turn_increment;
mod knockdown;
mod end;

pub mod util;

pub use initiate::*;
pub use action_select::*;
pub use command_select::*;
pub use target_select::*;
pub use event_register::*;
pub use rest_start::*;
pub use event_execute::*;
pub use turn_increment::*;
pub use knockdown::*;
pub use end::*;

use std::rc::Rc;
use crate::{BattleEnv, Participant};
use crate::event::{Event, ExecutionEvent, EventLog};
use crate::cmd_skill::CommandSkill;

//どのStateかを表すenum
pub enum StateEnum {
    PostInitiate(PostInitiateState), //開始直後
    PreActionSelect(PreActionSelectState), //行動選択前
    CommandSelect(CommandSelectState), //コマンド選択
    TargetSelect(TargetSelectState), //コマンド使用対象選択
    TargetConfirm(TargetConfirmState), //コマンド使用対象選択（対象が固定の場合）
    PreEventRegister(PreEventRegisterState), //イベントTL登録前
    PostEventRegister(PostEventRegisterState), //イベントTL登録後
    PreRestStart(PreRestStartState), //休むコマンドのタスク登録前
    PostRestStart(PostRestStartState), //休むコマンドのタスク登録前
    PreEventExecute(PreEventExecuteState), //イベント実行前
    PostEventExecute(PostEventExecuteState), //イベント実行後
    PreKnockdown(PreKnockdownState), //ノックダウン前
    PostKnockdown(PostKnockdownState), //ノックダウン後
    PreTurnIncrement(PreTurnIncrementState), //ターン経過前
    PostTurnIncrement(PostTurnIncrementState), //ターン経過後
    PreEnd(PreEndState), //終了直前
    PostEnd(PostEndState), //終了後
}

impl StateEnum {
    // オブジェクト作成ショートカット
    pub fn new_post_initiate(env: BattleEnv) -> StateEnum {
        StateEnum::PostInitiate(PostInitiateState::new(env))
    }

    pub fn new_pre_action_select(p: Participant, env: BattleEnv) -> StateEnum {
        StateEnum::PreActionSelect(PreActionSelectState::new(p, env))
    }

    pub fn new_command_select(p: Participant, env: BattleEnv) -> StateEnum {
        StateEnum::CommandSelect(CommandSelectState::new(p, env))
    }

    pub fn new_target_select(p: Participant, command: Rc<dyn CommandSkill>, env: BattleEnv) -> StateEnum {
        StateEnum::TargetSelect(TargetSelectState::new(p, command, env))
    }

    pub fn new_target_confirm(p: Participant, command: Rc<dyn CommandSkill>, env: BattleEnv) -> StateEnum {
        StateEnum::TargetConfirm(TargetConfirmState::new(p, command, env))
    }

    pub fn new_pre_event_register(executor: Participant, event: ExecutionEvent, env: BattleEnv) -> StateEnum {
        StateEnum::PreEventRegister(PreEventRegisterState::new(executor, event, env))
    }

    pub fn new_post_event_register(executor: Participant, event: Event, env: BattleEnv) -> StateEnum {
        StateEnum::PostEventRegister(PostEventRegisterState::new(executor, event, env))
    }

    pub fn new_pre_rest_start(executor: Participant, command: Rc<dyn CommandSkill>, env: BattleEnv) -> StateEnum {
        StateEnum::PreRestStart(PreRestStartState::new(executor, command, env))
    }

    pub fn new_post_rest_start(executor: Participant, env: BattleEnv) -> StateEnum {
        StateEnum::PostRestStart(PostRestStartState::new(executor, env))
    }

    pub fn new_pre_event_execute(env: BattleEnv) -> StateEnum {
        StateEnum::PreEventExecute(PreEventExecuteState::new(env))
    }

    pub fn new_post_event_execute(ev: Event, log: EventLog, env: BattleEnv) -> StateEnum {
        StateEnum::PostEventExecute(PostEventExecuteState::new(ev, log, env))
    }

    pub fn new_pre_knockdown(p: Participant, env: BattleEnv) -> StateEnum {
        StateEnum::PreKnockdown(PreKnockdownState::new(p, env))
    }

    pub fn new_post_knockdown(p: Participant, env: BattleEnv) -> StateEnum {
        StateEnum::PostKnockdown(PostKnockdownState::new(p, env))
    }

    pub fn new_pre_turn_increment(env: BattleEnv) -> StateEnum {
        StateEnum::PreTurnIncrement(PreTurnIncrementState::new(env))
    }

    pub fn new_post_turn_increment(env: BattleEnv) -> StateEnum {
        StateEnum::PostTurnIncrement(PostTurnIncrementState::new(env))
    }

    pub fn new_pre_end(result: BattleResult, env: BattleEnv) -> StateEnum {
        StateEnum::PreEnd(PreEndState::new(result, env))
    }

    pub fn new_post_end(result: BattleResult) -> StateEnum {
        StateEnum::PostEnd(PostEndState::new(result))
    }
}

#[derive(Clone)]
pub enum BattleResult {
    NotEnded,
    Draw,
    Canceled,
    Ended(u8), //決着がついた場合、勝ったグループのidを取得できる
}

impl BattleResult {
    pub fn is_ended(&self) -> bool {
        if let BattleResult::NotEnded = self {
            return false
        }
        true
    }
}