use crate::{BattleRunner, BattleEnv};
use super::*;

// 開始直後の状態
pub struct PostInitiateState {
    env: BattleEnv,
}

impl PostInitiateState {
    pub fn new(env: BattleEnv) -> Self {
        Self { env }
    }

    pub fn next(&self) -> BattleRunner {
        let next_state = util::get_next_state_common(&self.env);
        return BattleRunner::renew(self.env.clone(), next_state);
    }
}

