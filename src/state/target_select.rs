use std::rc::Rc;
use super::StateEnum;
use crate::{BattleRunner, BattleEnv, Participant};
use crate::cmd_skill::{CommandSkill, TargetType};
use crate::event::{ExecutionEvent};
//use crate::util;

// コマンド使用対象選択
pub struct TargetSelectState {
    executor: Participant,
    command: Rc<dyn CommandSkill>,
    env: BattleEnv,
}

impl TargetSelectState {
    pub fn new(executor: Participant, command: Rc<dyn CommandSkill>, env: BattleEnv) -> Self {
        Self { executor, command, env }
    }

    pub fn select(&self, i_target: usize) -> Result<BattleRunner, String> {
        let mut targets = Vec::new();
        let target_type = self.command.get_target_type();
        match &target_type {
            TargetType::Friend1 | TargetType::Enemy1 => {
                let selectable = target_type.get_selectable_participants(
                    self.executor.clone(), &self.env.borrow().participants
                );

                // インデックス範囲外のときはエラー
                // TODO: 選択可能な対象がいない場合はCommandSelectの段階で弾くように修正予定（そのような状況は今のところ想定されないが…）
                if selectable.len() <= i_target {
                    return Err("selected number is out of range.".to_string());
                }
                    
                let target = selectable[i_target].clone();
                targets.push(target);
            },
            _ => { return Err("unexpected invocation of this method !".to_string()); }
        }

        //let delay = self.command.get_delay() * 1;
        let event = ExecutionEvent::new(self.command.clone(), self.executor.clone(), targets);
        let next_state = StateEnum::new_pre_event_register(self.executor.clone(), event, self.env.clone());
        Ok(BattleRunner::renew(self.env.clone(), next_state))
    }

    // キャンセル
    pub fn cancel(&self) -> BattleRunner {
        let next_state = StateEnum::new_command_select(self.executor.clone(), self.env.clone());
        BattleRunner::renew(self.env.clone(), next_state)
    }

    pub fn get_executor(&self) -> Participant {
        self.executor.clone()
    }

    pub fn get_command(&self) -> Rc<dyn CommandSkill> {
        self.command.clone()
    }
}

//コマンド使用対象選択（対象が固定の場合）
pub struct TargetConfirmState {
    executor: Participant,
    command: Rc<dyn CommandSkill>,
    env: BattleEnv,
}

impl TargetConfirmState {
    pub fn new(executor: Participant, command: Rc<dyn CommandSkill>, env: BattleEnv) -> Self {
        Self { executor, command, env }
    }

    pub fn confirm(&self) -> Result<BattleRunner, String> {
        let mut targets = Vec::new();
        let target_type = self.command.get_target_type();
        match &target_type {
            TargetType::Myself => {
                targets.push(self.executor.clone());
            },
            TargetType::FriendAll | TargetType::EnemyAll => {
                target_type.get_selectable_participants(
                    self.executor.clone(), &self.env.borrow().participants
                ).drain(..).for_each(|p| targets.push(p));
            }
            _ => { return Err("unexpected invocation of this method !".to_string()); }
        }

        // コマンド実行イベントを参加者のTimelineに登録
        let event = ExecutionEvent::new(self.command.clone(), self.executor.clone(), targets);
        let next_state = StateEnum::new_pre_event_register(self.executor.clone(), event, self.env.clone());
        Ok(BattleRunner::renew(self.env.clone(), next_state))
    }

    // キャンセル
    pub fn cancel(&self) -> BattleRunner {
        let next_state = StateEnum::new_command_select(self.executor.clone(), self.env.clone());
        BattleRunner::renew(self.env.clone(), next_state)
    }

    pub fn get_executor(&self) -> Participant {
        self.executor.clone()
    }

    pub fn get_command(&self) -> Rc<dyn CommandSkill> {
        self.command.clone()
    }
}
