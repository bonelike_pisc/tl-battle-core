use std::rc::Rc;
use crate::{Participant, BattleEnv, BattleRunner};
use crate::cmd_skill::CommandSkill;
use crate::task::{Task, RestTask};
use super::{StateEnum, util};

pub struct PreRestStartState {
    executor: Participant,
    turns: u16,
    recovery_rate: u16,
    env: BattleEnv,
}

impl PreRestStartState {
    pub fn new(executor: Participant, command: Rc<dyn CommandSkill>, env: BattleEnv) -> Self {
        let turns = 10;
        let recovery_rate = command.get_cost() / turns;
        Self {
            executor, turns, recovery_rate, env
        }
    }

    pub fn next(&self) -> BattleRunner {
        let task = Task::new(RestTask::new(self.executor.clone(), self.turns, self.recovery_rate));
        self.executor.dynamic_ent.tasks.borrow_mut().add_task(task);
        let next_state = StateEnum::new_post_rest_start(self.executor.clone(), self.env.clone());
        BattleRunner::renew(self.env.clone(), next_state)
    }
}

pub struct PostRestStartState {
    executor: Participant,
    env: BattleEnv,
}

impl PostRestStartState {
    pub fn new(executor: Participant, env: BattleEnv) -> Self {
        Self { executor, env }
    }

    pub fn next(&self) -> BattleRunner {
        let next_state = util::get_next_state_common(&self.env);
        BattleRunner::renew(self.env.clone(), next_state)
    }

    pub fn get_executor(&self) -> &Participant {
        &self.executor
    }
}
