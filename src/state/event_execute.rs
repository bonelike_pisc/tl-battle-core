use crate::{BattleRunner, BattleEnv};
use super::*;
use crate::event::{Event, EventLog};

// イベント実行前
pub struct PreEventExecuteState {
    event: Event,
    env: BattleEnv
}

impl PreEventExecuteState {
    pub fn new(env: BattleEnv) -> Self {
        let mut _env = env;
        let event = _env.borrow_mut().event.pop().unwrap();
        Self { event, env: _env }
    }

    pub fn next(&mut self) -> BattleRunner {
        let log = self.event.execute(&mut self.env);
        let next_state = StateEnum::new_post_event_execute(self.event.clone(), log, self.env.clone());
        BattleRunner::renew(self.env.clone(), next_state)
    }

    pub fn get_event(&self) -> &Event {
        &self.event
    }
}

pub struct PostEventExecuteState {
    event: Event,
    log: EventLog,
    env: BattleEnv,
}

impl PostEventExecuteState {
    pub fn new(event: Event, log: EventLog, env: BattleEnv) -> Self {
        Self { event, log, env }
    }

    pub fn next(&self) -> BattleRunner {
        let next_state = util::get_next_state_common(&self.env);
        BattleRunner::renew(self.env.clone(), next_state)
    }

    pub fn get_event(&self) -> &Event {
        &self.event
    }

    pub fn get_log(&self) -> &EventLog {
        &self.log
    }
}

// 実行済みのイベントをタイムラインから削除
/*
fn remove_executed_event(event: Event, ent: &mut BattleEnv) {
    ent.borrow_mut().tl.borrow_mut().remove_event(event.clone());
    for p in &ent.borrow_mut().participants {
        p.dynamic_ent.borrow_mut().tl.remove_event(event.clone());
    }
}
*/
