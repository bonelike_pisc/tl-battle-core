use super::{StateEnum};
use crate::{BattleRunner, BattleEnv, Participant};
use crate::cmd_skill::{TargetType, CommandType};
//use crate::util;

pub struct CommandSelectState {
    executor: Participant,
    env: BattleEnv,
}

impl CommandSelectState {
    pub fn new(executor: Participant, env: BattleEnv) -> Self {
        Self { executor, env }
    }

    pub fn select(&self, i_command: usize) -> Result<BattleRunner, String> {
        let commands = self.executor.get_selectable_commands();

        // 使用可能なスキルがない場合は行動不可にする
        if commands.len() == 0 {
            return Err("no selectable commands.".to_string());
        }
        //インデックス範囲外の場合はエラー
        if commands.len() <= i_command {
            return Err(format!("index out of range. max: {}, specified: {}", commands.len()-1, i_command));
        }

        let selected = commands.get(i_command).unwrap().clone();
        //休むコマンド
        if let CommandType::Rest = selected.get_command_type() {
            let next_state = StateEnum::new_pre_rest_start(self.executor.clone(), selected, self.env.clone());
            return Ok(BattleRunner::renew(self.env.clone(), next_state));
        }
        //対象選択へ
        let next_state = match selected.get_target_type() {
            TargetType::Myself | TargetType::FriendAll | TargetType::EnemyAll => {
                StateEnum::new_target_confirm(self.executor.clone(), selected, self.env.clone())
            },
            TargetType::Friend1 | TargetType::Enemy1 => {
                StateEnum::new_target_select(self.executor.clone(), selected, self.env.clone())
            }
        };
        return Ok(BattleRunner::renew(self.env.clone(), next_state));
    }

    pub fn get_executor(&self) -> Participant {
        self.executor.clone()
    }
}
