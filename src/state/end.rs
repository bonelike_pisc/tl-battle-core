use crate::{BattleEnv, BattleRunner};
use super::{BattleResult, StateEnum};

// バトル終了前
pub struct PreEndState {
    result: BattleResult,
    env: BattleEnv,
}

impl PreEndState {
    pub fn new(result: BattleResult, env: BattleEnv) -> Self {
        Self { result, env }
    }

    pub fn next(&self) -> BattleRunner {
        //今のところは特に何もしないが、後で処理を追加するかも。
        let next_state = StateEnum::new_post_end(self.result.clone());
        BattleRunner::renew(self.env.clone(), next_state)
    }

    pub fn get_result(&self) -> &BattleResult {
        &self.result
    }
}

// バトル終了後
pub struct PostEndState {
    result: BattleResult,
}

impl PostEndState {
    pub fn new(result: BattleResult) -> Self {
        Self { result: result }
    }

    pub fn get_result(&self) -> &BattleResult {
        &self.result
    }
}
