use super::*;
use crate::{BattleRunner, BattleEnv, Participant};

// ノックダウン処理前
pub struct PreKnockdownState {
    p: Participant,
    env: BattleEnv,
}

impl PreKnockdownState {
    pub fn new(p: Participant, env: BattleEnv) -> Self {
        Self { p, env }
    }

    pub fn next(&mut self) -> BattleRunner {
        self.p.dynamic_ent.knockdown_flag.set(true);
        // イベントキューから対象が主体のイベントを削除
        self.env.borrow_mut().event.remove_by_subject(&self.p);
        let next_state = StateEnum::new_post_knockdown(self.p.clone(), self.env.clone());
        BattleRunner::renew(self.env.clone(), next_state)
    }

    pub fn get_target(&self) -> Participant {
        self.p.clone()
    }
}

// ノックダウン後処理
pub struct PostKnockdownState {
    p: Participant,
    env: BattleEnv,
}

impl PostKnockdownState {
    pub fn new(p: Participant, env: BattleEnv) -> Self {
        Self { p, env }
    }

    pub fn next(&self) -> BattleRunner {
        let next_state = util::get_next_state_common(&self.env);
        return BattleRunner::renew(self.env.clone(), next_state);
    }

    pub fn get_target(&self) -> Participant {
        self.p.clone()
    }
}
