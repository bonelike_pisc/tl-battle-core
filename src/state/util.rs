// stateモジュール内で使用するユーティリティ関数群

use crate::{BattleEnv, Participant};
use super::{StateEnum, BattleResult};

// ノックダウン処理を行うべき参加者がいれば1人取得
pub fn get_next_knockdown(ent: &BattleEnv) -> Option<Participant> {
    for p in &ent.borrow().participants {
        if !p.is_alive() && !p.dynamic_ent.knockdown_flag.get() {
            return Some(p.clone())
        }
    }
    None
}

// 終了しているかどうかをチェック
pub fn get_battle_result(env: &BattleEnv) -> BattleResult {
    let groups = env.get_participant_groups();
    let alive_groups: Vec<&u8> = groups.iter()
        .filter(|&g| env.group_is_alive(*g))
        .collect();      
    match alive_groups.len() {
        0 => BattleResult::Draw,
        1 => BattleResult::Ended(*alive_groups[0]),
        _ => BattleResult::NotEnded,
    }
}

/*
 * 次に遷移すべき状態を取得
 * 大抵のPost*Stateで同じ処理になるので、ここに共通化
 */
pub fn get_next_state_common(env: &BattleEnv) -> StateEnum {
    // ノックダウン判定
    if let Some(p) = get_next_knockdown(env) {
        return StateEnum::new_pre_knockdown(p, env.clone());
    }

    // 終了判定
    let result = get_battle_result(env);
    if result.is_ended() {
        return StateEnum::new_pre_end(result, env.clone());
    }

    // 次のイベントチェック 
    if env.borrow().event.has_some() {
        return StateEnum::new_pre_event_execute(env.clone());
    }

    // 行動選択可能キャラがいるかチェック
    let _env = env.borrow();
    let mut actable = _env.participants.iter().filter(|p| p.can_act());
    if let Some(p) = actable.next() {
        // コマンド選択可能な参加者がいればコマンド選択へ
        return StateEnum::new_pre_action_select(p.clone(), env.clone());
    }

    // コマンド選択可能な参加者がいなければターン更新へ
    return StateEnum::new_pre_turn_increment(env.clone());
}
