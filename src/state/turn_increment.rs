use super::{StateEnum, util};
use crate::{BattleRunner, BattleEnv};

// ターン切り替え前
pub struct PreTurnIncrementState {
    env: BattleEnv,
}

impl PreTurnIncrementState {
    pub fn new(env: BattleEnv) -> Self {
        Self { env }
    }

    pub fn next(&mut self) -> BattleRunner {
        self.env.get_participants().iter_mut().for_each(|p|
            if p.is_alive() {
                p.dynamic_ent.tasks.borrow_mut().update(&mut self.env);
            }
        );

        let next_state = StateEnum::new_post_turn_increment(self.env.clone());
        BattleRunner::renew(self.env.clone(), next_state)
    }
}

// ターン切り替え後
pub struct PostTurnIncrementState {
    env: BattleEnv
}

impl PostTurnIncrementState {
    pub fn new(env: BattleEnv) -> Self {
        Self { env }
    }

    pub fn next(&self) -> BattleRunner {
        let next_state = util::get_next_state_common(&self.env);
        BattleRunner::renew(self.env.clone(), next_state)
    }
}
