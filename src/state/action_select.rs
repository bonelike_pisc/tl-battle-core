use super::*;
use crate::{BattleRunner, BattleEnv, Participant};
use crate::event::ExecutionEvent;
use crate::ai::RandomPicker;
use crate::cmd_skill::CommandType;

pub struct PreActionSelectState {
    executor: Participant,
    env: BattleEnv
}

impl PreActionSelectState {
    pub fn new(executor: Participant, env: BattleEnv) -> Self {
        Self { executor, env }
    }

    pub fn next(&self) -> Result<BattleRunner, String> {
        if self.executor.static_ent.is_playable {
            //操作キャラの場合
            let next_state = StateEnum::new_command_select(self.executor.clone(), self.env.clone());
            return Ok(BattleRunner::renew(self.env.clone(), next_state));
        }

        //自動選択の場合
        let ai = self.executor.static_ent.ai_pattern.as_ref().unwrap();
        let prob_table = ai.generate_probabilities(self.executor.clone(), self.env.clone());

        if prob_table.is_empty() {
            // できることがない場合は、何もせず1ターン経過させる（TODO）
        }

        let mut random = RandomPicker::default();
        let choice = random.pick(&prob_table)?;
        //休むコマンドの場合
        if let CommandType::Rest = choice.command.get_command_type() {
            let next_state = StateEnum::new_pre_rest_start(self.executor.clone(), choice.command, self.env.clone());
            return Ok(BattleRunner::renew(self.env.clone(), next_state));
        }

        //let delay = choice.command.get_delay() * 1;
        let event = ExecutionEvent::new(choice.command.clone(), self.executor.clone(), choice.targets.clone());

        let next_state = StateEnum::new_pre_event_register(self.executor.clone(), event, self.env.clone());
        Ok(BattleRunner::renew(self.env.clone(), next_state))
    }

    pub fn get_executor(&self) -> Participant {
        self.executor.clone()
    }
}
