use crate::{BattleRunner, BattleEnv, Participant};
use crate::event::{Event, ExecutionEvent};
use crate::task::{Task, ExecutionPrepareTask};
use super::*;

pub struct PreEventRegisterState {
    executor: Participant,
    event: Event,
    env: BattleEnv,
}

impl PreEventRegisterState {
    pub fn new(executor: Participant, event: ExecutionEvent, env: BattleEnv) -> Self {
        Self {
            executor,
            event: Event::new(event),
            env,
        }
    }

    pub fn next(&self) -> BattleRunner {
        let task = Task::new(ExecutionPrepareTask::new(self.event.clone()));
        self.executor.dynamic_ent.tasks.borrow_mut().add_task(task);

        let next_state = StateEnum::new_post_event_register(
            self.executor.clone(),
            self.event.clone(),
            self.env.clone()
        );
        BattleRunner::renew(self.env.clone(), next_state)
    }

    pub fn get_executor(&self) -> &Participant {
        &self.executor
    }

    pub fn get_event(&self) -> &Event {
        &self.event
    }
}

pub struct PostEventRegisterState {
    executor: Participant,
    event: Event,
    env: BattleEnv,
}

impl PostEventRegisterState {
    pub fn new(executor: Participant, event: Event, env: BattleEnv) -> Self {
        Self {
            executor,
            event,
            env,
        }
    }

    pub fn next(&self) -> BattleRunner {
        let next_state = util::get_next_state_common(&self.env);
        BattleRunner::renew(self.env.clone(), next_state)
    }

    pub fn get_executor(&self) -> &Participant {
        &self.executor
    }

    pub fn get_event(&self) -> &Event {
        &self.event
    }
}
