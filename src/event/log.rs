//use std::rc::Rc;
use crate::Participant;

// TimelineEventの実行結果
pub struct EventLog {
    elms: Vec<EventLogElement>
}

impl EventLog {
    pub fn new() -> Self {
        Self { elms: Vec::new() }
    }

    pub fn get(&self) -> &Vec<EventLogElement> {
        &self.elms
    }

    pub fn take(self) -> Vec<EventLogElement> {
        self.elms
    }

    pub fn append(&mut self, elm: EventLogElement) {
        self.elms.push(elm);
    }

    pub fn concatenate(&mut self, other: EventLog) {
        other.take().drain(..).for_each(|e| self.elms.push(e));
    }
}

// TimelineEvent実行時に起こった個々の事象
pub enum EventLogElement {
    Damage(Participant, u16),
    Recover(Participant, u16),
    SkipTarget(Participant), //対象への作用をスキップ（対象が既にノックダウンしているときなど）
    Failure, //発動失敗
}
