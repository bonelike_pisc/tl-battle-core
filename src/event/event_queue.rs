use std::collections::VecDeque;
use super::Event;
use crate::Participant;

pub struct EventQueue {
    inner: VecDeque<Event>,
}

impl EventQueue {
    pub fn new() -> Self {
        Self {
            inner: VecDeque::new()
        }
    }

    pub fn ref_inner(&self) -> &VecDeque<Event> {
        &self.inner
    }

    pub fn push(&mut self, e: Event) {
        self.inner.push_back(e);
    }

    pub fn pop(&mut self) -> Option<Event> {
        self.inner.pop_front()
    }

    pub fn has_some(&self) -> bool {
        self.inner.len() > 0
    }

    pub fn remove_by_subject(&mut self, subject: &Participant) {
        self.inner.retain(|e| {
            match e.subject() {
                Some(s) => &s != subject,
                None => true
            }
        })
    }
}