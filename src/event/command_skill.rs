use std::rc::Rc;
use super::{TimelineEvent, TimelineEventType, EventLog};
use crate::Participant;
use crate::cmd_skill::CommandSkill;

pub struct CommandSkillEvent {
    cmd: Rc<dyn CommandSkill>,
    executor: Participant,
    targets: Vec<Participant>
}

impl CommandSkillEvent {
    pub fn new(
        cmd: Rc<dyn CommandSkill>,
        executor: Participant,
        targets: Vec<Participant>
    ) -> Self {
        Self {
            cmd: cmd,
            executor: executor,
            targets: targets
        }
    }
}

impl TimelineEvent for CommandSkillEvent {
    fn execute(&mut self) -> EventLog {
        let mut log = EventLog::new();
        //対象数が1以上のとき
        for target in &mut self.targets {
            if !target.is_alive() {
                continue;
            }
            let log_tmp = self.cmd.execute(
                &mut self.executor,
                Some(target)
            );
            log.concatenate(log_tmp);
        }

        //対象数が0のときは対象なしで1度だけ実行
        if self.targets.len() == 0 {
            let log_tmp = self.cmd.execute(
                &mut self.executor,
                None
            );
            log.concatenate(log_tmp);
        }

        //使用者をコマンド受付可能状態にする
        self.executor.let_free();
        log
    }

    fn get_info(&self) -> TimelineEventType {
        TimelineEventType::CommandSkill(
            self.cmd.clone(),
            self.executor.clone(),
            self.targets.clone()
        )
    }

    fn get_description(&self) -> String {
        let cmd_name = self.cmd.get_name();
        let executor_name = &self.executor.static_ent.name;
        String::from(cmd_name) + "|" + executor_name
    }
}
