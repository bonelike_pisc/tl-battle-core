//mod command_skill;
mod log;
mod event_queue;
mod execution;
//pub use command_skill::*;
pub use log::*;
pub use event_queue::*;
pub use execution::*;

use std::rc::Rc;
use std::cell::RefCell;
use crate::cmd_skill::CommandSkill;
use crate::Participant;
use crate::battle::BattleEnv;

// バトル中に発動する種々のイベントのtrait
pub trait EventBase {
    fn execute(&mut self, env: &mut BattleEnv) -> EventLog; //発動時の処理
    fn cancel(&mut self, env: &mut BattleEnv) -> EventLog; //キャンセルされたときの処理
    fn get_info(&self) -> EventInfo; //具象型の情報を取得
}

// EventBaseのラッパーオブジェクト
#[derive(Clone)]
pub struct Event {
    e: Rc<RefCell<dyn EventBase>>
}

impl Event {
    pub fn new<E>(e: E) -> Self where E: EventBase + 'static {
        Self {
            e: Rc::new(RefCell::new(e))
        }
    }

    pub fn execute(&mut self, env: &mut BattleEnv) -> EventLog {
        self.e.borrow_mut().execute(env)
    }

    pub fn cancel(&mut self, env: &mut BattleEnv) -> EventLog {
        self.e.borrow_mut().cancel(env)
    }

    pub fn get_info(&self) -> EventInfo {
        self.e.borrow().get_info()
    }

    pub fn subject(&self) -> Option<Participant> { //主体
        match self.get_info() {
            EventInfo::Execution(_cmd, executor, _targets) => Some(executor),
            _ => None,
        }
    }
}

impl PartialEq for Event {
    fn eq(&self, other: &Self) -> bool {
        Rc::ptr_eq(&self.e, &other.e)
    }
}

// イベントの具象オブジェクトの情報（型とメンバ）
pub enum EventInfo {
    //TODO: to be append elements
    Execution(Rc<dyn CommandSkill>, Participant, Vec<Participant>),
}
