use std::rc::Rc;
use super::{EventBase, EventInfo, EventLog};
use crate::cmd_skill::CommandSkill;
use crate::Participant;
use crate::battle::BattleEnv;

pub struct ExecutionEvent {
    cmd: Rc<dyn CommandSkill>,
    executor: Participant,
    targets: Vec<Participant>
}

impl ExecutionEvent {
    pub fn new(cmd: Rc<dyn CommandSkill>, executor: Participant, targets: Vec<Participant>) -> Self {
        Self {
            cmd, executor, targets
        }
    }
}

impl EventBase for ExecutionEvent {
    fn execute(&mut self, env: &mut BattleEnv) -> EventLog {
        let log = self.cmd.execute(
            &mut self.executor,
            &mut self.targets,
            env
        );

        log
    }

    fn cancel(&mut self, _env: &mut BattleEnv) -> EventLog {
        EventLog::new() //TODO: cancelされた旨のログを返す
    }

    fn get_info(&self) -> EventInfo {
        EventInfo::Execution(self.cmd.clone(), self.executor.clone(), self.targets.clone())
    }
}