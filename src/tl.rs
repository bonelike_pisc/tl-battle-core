use std::rc::Rc;
use std::collections::HashMap;
use crate::event::TimelineEvent;

pub struct Timeline {
    // キーにHashが実装されている必要があるため、整数値をキーにしてeventとcountdownを対応付ける
    events: HashMap<u8, Rc<dyn TimelineEvent>>,
    countdowns: HashMap<u8, u16>
}

impl Timeline {
    pub fn new() -> Self {
        Self {
            events: HashMap::new(),
            countdowns: HashMap::new()
        }
    }

    // eventを追加
    pub fn new_event(&mut self, event: Rc<dyn TimelineEvent>, countdown: u16) {
        let key = self.generate_key().unwrap();
        self.events.insert(key, event);
        self.countdowns.insert(key, countdown);
    }

    // eventを削除
    pub fn remove_event(&mut self, event: Rc<dyn TimelineEvent>) {
        if let Some(k) = self.find_key(event) {
            self.events.remove(&k);
            self.countdowns.remove(&k);
        }
    }

    // 全eventを削除
    pub fn clear(&mut self) {
        self.events.clear();
        self.countdowns.clear();
    }

    // 各eventのcountdown値をデクリメント
    pub fn countdown(&mut self) {
        // countdown値がすでに0になっている要素を削除
        for key in self.get_zerocount_keys() {
            self.events.remove(&key);
            self.countdowns.remove(&key);
        }

        // countdown値をデクリメント
        for key in self.get_keys() {
            let now_value = *self.countdowns.get(&key).unwrap();
            self.countdowns.insert(key, now_value - 1);
        }
    }

    pub fn get_all_events(&self) -> Vec<EventEntry> {
        let mut ret = Vec::new();
        for key in self.get_keys() {
            let ev = self.events.get(&key).unwrap();
            let countdown = self.countdowns.get(&key).unwrap();
            ret.push(EventEntry { event: ev.clone(), countdown: *countdown });
        }
        ret
    }

    //TODO: ゼロカウントとなったイベント一覧の取得メソッド(pub)
    pub fn get_zerocount_events(&self) -> Vec<Rc<dyn TimelineEvent>> {
        let mut ret = Vec::new();
        for key in self.get_zerocount_keys() {
            let ev = self.events.get(&key).unwrap();
            ret.push(ev.clone());
        }
        ret
    }

    // 新規キー値を生成
    fn generate_key(&self) -> Option<u8> {
        for i in 0..std::u8::MAX {
            if !self.events.contains_key(&i) {
                return Option::Some(i)
            }
        }
        Option::None
    }

    // キー一覧を取得（借用チェッカ回避のため新規Vecを作成）
    fn get_keys(&self) -> Vec<u8> {
        let mut ret = Vec::new();
        self.events.keys().for_each(|k| {
            ret.push(*k);
        });
        ret
    }

    // countdownがゼロになっているキー一覧を取得
    fn get_zerocount_keys(&self) -> Vec<u8> {
        let mut ret = Vec::new();
        self.events.keys()
        .filter(|&k| *self.countdowns.get(k).unwrap() == 0)
        .for_each(|k| ret.push(*k));
        ret
    }

    // eventに対応するキーを検索
    fn find_key(&self, event: Rc<dyn TimelineEvent>) -> Option<u8> {
        self.events.keys()
        .filter(|k| {
            Rc::ptr_eq(&event, self.events.get(k).unwrap())
        })
        .next()
        .map(|k| *k)
    }

}

// eventとcountdown値のペア
pub struct EventEntry {
    pub event: Rc<dyn TimelineEvent>,
    pub countdown: u16
}