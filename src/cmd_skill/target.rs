use std::rc::Rc;
use crate::participant::Participant;

// コマンドスキルに追加で適用するスキル使用対象のフィルタ
pub trait TargetFilter {
    // 行動選択時（対象に含める場合はtrue。デフォルトは全員対象）
    fn select_time(&self, _p: Rc<Participant>) -> bool { true }

    // 発動時（対象に含める場合はtrue。デフォルトは全員対象）
    fn execute_time(&self, _p: Rc<Participant>) -> bool { true }

    fn filter_for_select(&self, ps: &Vec<Rc<Participant>>) -> Vec<Rc<Participant>> {
        ps.iter().filter(|&p| {
            self.select_time(p.clone())
        }).cloned().collect::<Vec<Rc<Participant>>>()
    }

    fn filter_for_execute(&self, ps: &Vec<Rc<Participant>>) -> Vec<Rc<Participant>> {
        ps.iter().filter(|&p| {
            self.execute_time(p.clone())
        }).cloned().collect::<Vec<Rc<Participant>>>()
    }
}

//生存している者を対象とするフィルタ
pub struct SimpleFilter;

impl TargetFilter for SimpleFilter {
    fn select_time(&self, p: Rc<Participant>) -> bool {
        p.is_alive()
    }

    fn execute_time(&self, p: Rc<Participant>) -> bool {
        p.is_alive()
    }
}
