mod recover;
mod attack;
mod attack2;
mod rest;
//pub mod target;
pub use recover::*;
pub use attack::*;
pub use attack2::*;
pub use rest::*;

//use std::rc::Rc;
use crate::participant::Participant;
use crate::event::{EventLog};
use crate::battle::BattleEnv;

pub trait CommandSkill {
    fn get_name(&self) -> &String;
    fn get_cost(&self) -> u16; //cost値を取得
    fn execute(&self, executor: &mut Participant, targets: &mut Vec<Participant>, env: &mut BattleEnv) -> EventLog; //発動
    fn get_target_type(&self) -> &TargetType;
    fn get_command_type(&self) -> CommandType;
}

pub enum CommandType {
    Attack, //攻撃
    Support, //補助
    Rest, //休む
}

pub enum TargetType {
    Myself, //自分
    Friend1, //味方1人（自分含む）
    FriendAll, //味方全員（自分含む）
    Enemy1, //敵1人
    EnemyAll, //敵全体
}

impl TargetType {
    // 対象となりうる参加者一覧を取得
    pub fn get_selectable_participants(
        &self, executor: Participant, participants: &Vec<Participant>
    ) -> Vec<Participant> {
        match &self {
            Self::Myself => {
                vec![executor.clone()]
            }
            Self::Friend1 | Self::FriendAll => {
                participants.iter()
                .filter(|&p| Participant::is_same_group(&executor, p))
                .filter(|&p| p.is_alive())
                .cloned()
                .collect()
            }
            Self::Enemy1 | Self::EnemyAll => {
                participants.iter()
                .filter(|&p| ! Participant::is_same_group(&executor, p))
                .filter(|&p| p.is_alive())
                .cloned()
                .collect()
            }
        }
    }
}