//use std::rc::Rc;
use crate::participant::Participant;
use super::{CommandSkill, TargetType, CommandType};
use crate::event::{EventLog, EventLogElement};
use crate::battle::BattleEnv;
//use super::target::{TargetFilter, SimpleFilter};

// 対象のHPを減らす
fn damage(target: &mut Participant, hp: u16) {
    let now_hp = target.get_hp();
    let next_hp = if now_hp <= hp { 0 } else {now_hp - hp};
    target.set_hp(next_hp);
}

// 敵1体を攻撃するコマンド（ダメージは固定値）
pub struct AttackConstantDamage {
    name: String,
    cost: u16,
    hp: u16,
    target: TargetType,
}

impl AttackConstantDamage {
    pub fn new(name: String, cost: u16, hp: u16) -> Self {
        Self {
            name,
            cost,
            hp,
            target: TargetType::Enemy1,
        }
    }

    pub fn enemy_all(name: String, cost: u16, hp: u16) -> Self {
        Self {
            name,
            cost,
            hp,
            target: TargetType::EnemyAll,
        }
    }
}

impl CommandSkill for AttackConstantDamage {
    fn get_name(&self) -> &String {
        &self.name
    }

    fn get_cost(&self) -> u16 {
        self.cost
    }

    fn execute(&self, _executor: &mut Participant, targets: &mut Vec<Participant>, _env: &mut BattleEnv) -> EventLog {
        let mut log = EventLog::new();
        for t in targets {
            if ! t.is_alive() {
                log.append(EventLogElement::SkipTarget(t.clone()));
                continue;
            }
            damage(t, self.hp);
            log.append(EventLogElement::Damage(t.clone(), self.hp));
        }
        log
    }

    fn get_target_type(&self) -> &TargetType {
        &self.target
    }

    fn get_command_type(&self) -> CommandType {
        CommandType::Attack
    }
}

// 敵1体を攻撃するコマンド（ダメージをパラメータで計算）
pub struct AttackStandard {
    name: String,
    cost: u16,
    power: u16,
    target: TargetType,
}

impl AttackStandard {
    pub fn new(name: String, cost: u16, power: u16) -> Self {
        Self {
            name,
            cost,
            power,
            target: TargetType::Enemy1,
        }
    }

    pub fn enemy_all(name: String, cost: u16, power: u16) -> Self {
        Self {
            name,
            cost,
            power,
            target: TargetType::EnemyAll,
        }
    }
}

impl CommandSkill for AttackStandard {
    fn get_name(&self) -> &String {
        &self.name
    }

    fn get_cost(&self) -> u16 {
        self.cost
    }

    fn execute(&self, executor: &mut Participant, targets: &mut Vec<Participant>, _env: &mut BattleEnv) -> EventLog {
        let mut log = EventLog::new();
        for t in targets {
            if ! t.is_alive() {
                log.append(EventLogElement::SkipTarget(t.clone()));
                continue;
            }
            let damage_value = self.power as f32 * exp_p2(executor.static_ent.power as f32) / exp_p2(t.static_ent.defence as f32);
            damage(t, damage_value as u16);
            log.append(EventLogElement::Damage(t.clone(), damage_value as u16));
        }
        log
    }

    fn get_target_type(&self) -> &TargetType {
        &self.target
    }

    fn get_command_type(&self) -> CommandType {
        CommandType::Attack
    }
}

// 魔力・耐久力の値のダメージ量への影響度を表す関数
fn exp_p2(x: f32) -> f32 {
    1.0 + x + x * x / 2.0
}
