use super::{CommandSkill, TargetType, CommandType};
use crate::{Participant, BattleEnv};
use crate::event::EventLog;

// 休んでスタミナを回復させる特殊なコマンド
pub struct Rest {
    turns: u16, //休むターン数
    recovery_rate: u16, //1ターンあたりのスタミナ回復量
    name: String,
    target_type: TargetType,
}

impl Rest {
    pub fn new(recovery_rate: u16) -> Self {
        Self {
            turns: 10, //とりあえず固定
            recovery_rate,
            name: "休む".to_string(),
            target_type: TargetType::Myself,
        }
    }
}

impl CommandSkill for Rest {
    fn get_name(&self) -> &String {
        &self.name
    }

    fn get_cost(&self) -> u16 {
        self.turns * self.recovery_rate
    }

    fn execute(&self, _executor: &mut Participant, _targets: &mut Vec<Participant>, _env: &mut BattleEnv) -> EventLog {
        //何もしない（というか呼ばれない想定）
        EventLog::new()
    }

    fn get_target_type(&self) -> &TargetType {
        &self.target_type
    }

    fn get_command_type(&self) -> CommandType {
        CommandType::Rest
    }
}