//use std::rc::Rc;
use crate::participant::Participant;
use super::{CommandSkill, TargetType, CommandType};
use crate::event::{EventLog, EventLogElement};
use crate::battle::BattleEnv;
//use super::target::{TargetFilter, SimpleFilter};

// 回復
fn recover(target: &mut Participant, hp: u16) {
    let now_hp = target.get_hp();
    let next_hp = if now_hp + hp > 1000 { 1000 } else {now_hp + hp};
    target.set_hp(next_hp);
}

// 自分を回復するコマンド（回復量は固定値）
pub struct RecoverConstant {
    name: String,
    cost: u16,
    hp: u16,
    target: TargetType,
}

impl RecoverConstant {
    pub fn myself(name: String, cost: u16, hp: u16) -> Self {
        Self {
            name,
            cost,
            hp,
            target: TargetType::Myself,
        }
    }

    pub fn friend1(name: String, cost: u16, hp: u16) -> Self {
        Self {
            name,
            cost,
            hp,
            target: TargetType::Friend1,
        }
    }

    pub fn friend_all(name: String, cost: u16, hp: u16) -> Self {
        Self {
            name,
            cost,
            hp,
            target: TargetType::FriendAll,
        }
    }
}

impl CommandSkill for RecoverConstant {
    fn get_name(&self) -> &String {
        &self.name
    }

    fn get_cost(&self) -> u16 {
        self.cost
    }

    fn execute(&self, _executor: &mut Participant, targets: &mut Vec<Participant>, _env: &mut BattleEnv) -> EventLog {
        let mut log = EventLog::new();
        for t in targets {
            if ! t.is_alive() {
                log.append(EventLogElement::SkipTarget(t.clone()));
                continue;
            }
            recover(t, self.hp);
            log.append(EventLogElement::Recover(t.clone(), self.hp));
        }
        log
    }

    fn get_target_type(&self) -> &TargetType {
        &self.target
    }

    fn get_command_type(&self) -> CommandType {
        CommandType::Support
    }
}
