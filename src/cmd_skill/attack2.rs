use crate::participant::Participant;
use super::{CommandSkill, TargetType, CommandType};
use crate::event::{EventLog, EventLogElement};
use crate::battle::BattleEnv;
use crate::util::{numeric, random};
//use super::target::{TargetFilter, SimpleFilter};

// 対象のHPを減らす
fn damage(target: &mut Participant, hp: u16) {
    let now_hp = target.get_hp();
    let next_hp = if now_hp <= hp { 0 } else {now_hp - hp};
    target.set_hp(next_hp);
}

// 敵1体を攻撃するコマンド（ダメージをパラメータで計算）
pub struct AttackVariant {
    name: String,
    cost: u16, //所要コスト
    power: u16, //威力
    required_skill_level: u16, //要求技量
    target: TargetType,
}

impl AttackVariant {
    pub fn new(name: String, cost: u16, power: u16, required_skill_level: u16) -> Self {
        Self {
            name,
            cost,
            power,
            required_skill_level,
            target: TargetType::Enemy1,
        }
    }

    pub fn enemy_all(name: String, cost: u16, power: u16, required_skill_level: u16) -> Self {
        Self {
            name,
            cost,
            power,
            required_skill_level,
            target: TargetType::EnemyAll,
        }
    }
}

impl CommandSkill for AttackVariant {
    fn get_name(&self) -> &String {
        &self.name
    }

    fn get_cost(&self) -> u16 {
        self.cost
    }

    fn execute(&self, executor: &mut Participant, targets: &mut Vec<Participant>, _env: &mut BattleEnv) -> EventLog {
        let mut log = EventLog::new();

        // 発動成功判定
        let relative_skill_level = executor.static_ent.skill_level as f32 / self.required_skill_level as f32;
        let prob_success = numeric::generate_sigmoid(0.5, 0.0, 1.0, 1.0).unwrap()(relative_skill_level);
        let is_success = random::get_random_tf(prob_success);
        if ! is_success {
            log.append(EventLogElement::Failure);
            return log;
        }

        for t in targets {
            if ! t.is_alive() {
                log.append(EventLogElement::SkipTarget(t.clone()));
                continue;
            }
            
            // ダメージ算出
            let power_min = self.power as f32 * numeric::generate_sigmoid(0.5, 0.5, 1.0, 0.9).unwrap()(relative_skill_level);
            let power_max = self.power as f32;
            let actual_power = random::get_random_value_between_range(power_min, power_max);
            let damage_value = actual_power * 100.0 / t.static_ent.defence as f32;
            damage(t, damage_value as u16);
            log.append(EventLogElement::Damage(t.clone(), damage_value as u16));
        }
        log
    }

    fn get_target_type(&self) -> &TargetType {
        &self.target
    }

    fn get_command_type(&self) -> CommandType {
        CommandType::Attack
    }
}

